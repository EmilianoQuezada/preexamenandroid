package com.example.preexamencorte1;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;

import java.util.Random;


public class reciboNominaActivity extends AppCompatActivity {
    private EditText txtHorasNormal, txtHorasExtra, txtSubTotal, txtImpuesto, txtTotal;
    private RadioGroup radioGroupPuesto;

    private RadioButton rdbAuxiliar;
    private TextView lblNumRecibo, lblNombre, lblNombre2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        //Inicializar componentes
        txtHorasNormal = findViewById(R.id.txtHorasNormal);
        txtHorasExtra = findViewById(R.id.txtHorasExtras);
        txtSubTotal = findViewById(R.id.txtSubTotal);
        txtImpuesto = findViewById(R.id.txtImpuesto);
        txtTotal = findViewById(R.id.txtTotal);
        radioGroupPuesto = findViewById(R.id.radioGroupPuesto);
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar);
        lblNumRecibo = findViewById(R.id.lblNumRecibo);
        lblNombre = findViewById(R.id.lblNombre);
        lblNombre2 = findViewById(R.id.lblNombre2);
        Button btnCalcular = findViewById(R.id.btnCalcular);
        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        Button btnRegresar = findViewById(R.id.btnRegresar);

        // Recibir datos del Intent
        String nombre = getIntent().getStringExtra("nombre");
        int numRecibo = generateFolioNumber();

        // Establecer los datos recibidos en los campos correspondientes
        lblNombre.setText(nombre);
        lblNombre2.setText(nombre);
        lblNumRecibo.setText(String.valueOf(numRecibo));
        lblNumRecibo.setEnabled(false);
        lblNombre.setEnabled(false);
        lblNombre2.setEnabled(false);

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int numRecibo = Integer.parseInt(lblNumRecibo.getText().toString());
                String nombre = lblNombre.getText().toString();
                int horasNormales = Integer.parseInt(txtHorasNormal.getText().toString());
                int horasExtras = Integer.parseInt(txtHorasExtra.getText().toString());
                int puesto = getPuestoSeleccionado(radioGroupPuesto);
                float porcentajeImpuesto = 16.0f;

                ReciboNomina recibo = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtras, puesto, porcentajeImpuesto);
                double subtotal = recibo.calcularSubtotal();
                double impuesto = recibo.calcularImpuesto();
                double totalAPagar = recibo.calcularTotal();

                txtSubTotal.setText("$"+String.valueOf(subtotal));
                txtImpuesto.setText("$"+String.valueOf(impuesto));
                txtTotal.setText("$"+String.valueOf(totalAPagar));
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtHorasNormal.setText("");
                txtHorasExtra.setText("");
                radioGroupPuesto.clearCheck();
                rdbAuxiliar.setChecked(true);
                txtSubTotal.setText("$");
                txtImpuesto.setText("$");
                txtTotal.setText("$");
            }
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private int generateFolioNumber() {
        Random random = new Random();
        return random.nextInt(9000) + 1000; // Genera un número aleatorio de 4 dígitos
    }

    private int getPuestoSeleccionado(RadioGroup radioGroup) {
        int radioButtonID = radioGroup.getCheckedRadioButtonId();
        View radioButton = radioGroup.findViewById(radioButtonID);
        int idx = radioGroup.indexOfChild(radioButton);

        // Adjust index based on your position values
        // Assuming Auxiliar = 1, Albañil = 2, Ing. Obra = 3
        return idx + 1;
    }
}
