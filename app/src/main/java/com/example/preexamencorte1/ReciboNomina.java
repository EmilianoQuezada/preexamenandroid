package com.example.preexamencorte1;

public class ReciboNomina {
    private  int numRecibo;
    private  String Nombre;
    private  float horasTrabNormal;
    private  float horasTrabExtra;
    private  int puesto;
    private  float impuestoPorc;

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtra, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.Nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtra = horasTrabExtra;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public ReciboNomina() {
        this.numRecibo=0;
        this.Nombre="";
        this.horasTrabNormal= 0.0f;
        this.horasTrabExtra= 0.0f;
        this.puesto=0;
        this.impuestoPorc=0.0f;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtra() {
        return horasTrabExtra;
    }

    public void setHorasTrabExtra(float horasTrabExtra) {
        this.horasTrabExtra = horasTrabExtra;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal(){
        float base = 200;
        float subtotal;

        if (puesto==1){
            base = (float) (base + (base*.20));
        }

        else if (puesto==2) {
            base = (float) (base + (base*.50));
        }
        else if (puesto==3) {
            base = (float) (base + (base*.100));
        }

        subtotal = (float) (base*horasTrabNormal) + (horasTrabExtra * (base*2));
        return subtotal;
    }

    public float calcularImpuesto(){
        impuestoPorc = 0.16f;
        float impuesto;
        impuesto = (float) calcularSubtotal() * (impuestoPorc);
        return  impuesto;
    }

    public float calcularTotal(){
        float total;
        total = (float) calcularSubtotal() - calcularImpuesto();
        return total;
    }
}
